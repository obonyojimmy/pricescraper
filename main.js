// Grab links from Google.
var Horseman = require("node-horseman");

var horseman = new Horseman();

var links = [];

function getLinks(){
	return horseman.evaluate( function(){
		// This code is executed in the browser.
		var links = [];
		$("div.calc-results div.plan-row").each(function( item ){
			var link = {
        subscribers : $(this).find('div.col-subscribers').text(),
        emails : $(this).find('div.col-emails').text(),
        fee : $(this).find('div.col-fee').text(),
      }
			links.push(link);
		});
		return links;
	});
}

function scrape(){

	return new Promise( function( resolve, reject ){
		return getLinks()
		.then(function(newLinks){
			links = links.concat(newLinks);
		})
		.then( resolve );
	});
}

horseman
	.userAgent("Mozilla/5.0 (Windows NT 6.1; WOW64; rv:27.0) Gecko/20100101 Firefox/27.0")
	.open("https://mailchimp.com/pricing/growing-business/")
	.waitForSelector("div.calc-results")
	.then( scrape )
	.finally(function(){
		console.log(links.length)
    var output = JSON.stringify(links)
		console.log(output)
		horseman.close();
	});

